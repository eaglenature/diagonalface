// & g++ -std=c++11 main.cpp && ./a.out
#include <iostream>
#include <memory>
#include <vector>

struct IDiagonalass
{
    virtual ~IDiagonalass() = default;

    virtual double Diagonalize(const std::vector<double>& a, const std::vector<double>& b) = 0;
    virtual void Configure(const std::string& ham, std::size_t up, std::size_t down, std::size_t steps) = 0;
    virtual void PrintStats() = 0;
};

struct Diagonalass : IDiagonalass
{
    Diagonalass(std::size_t n) : dummy(n) {}

    void Configure(const std::string& ham, std::size_t up, std::size_t down, std::size_t steps) override
    {
        std::cout << "Diagonalass::configure()" << std::endl;
        std::cout << "           ham: " << ham << std::endl;
        std::cout << "            up: " << up << std::endl;
        std::cout << "          down: " << down << std::endl;
        std::cout << "         steps: " << steps << std::endl;
        // lazy configuration code moved from constructor ...
    }
    double Diagonalize(const std::vector<double>& a, const std::vector<double>& b) override
    {
        std::cout << "Diagonalass::diagonalize(" << a.size() << ", " << b.size() << ")" << std::endl;
        // diagonalization code ...
        return dummy*3.14;
    }
    void PrintStats() override
    {
        std::cout << "Diagonalass::print_stats()" << std::endl;
    }
    std::size_t dummy;
};

struct Config
{
    Config& set_ham(const std::string& h)
    {
        ham = h;
        return *this;
    }

    Config& set_up(std::size_t u)
    {
        up = u;
        return *this;
    }

    Config& set_down(std::size_t d)
    {
        down = d;
        return *this;
    }

    Config& set_steps(std::size_t s)
    {
        steps = s;
        return *this;
    }        

    std::string ham;
    std::size_t up;
    std::size_t down;
    std::size_t steps;
};

struct Params
{
    Params& set_one_body(std::vector<double> a)
    {
        one_body = std::move(a);
        return *this;
    }
    Params& set_two_body(std::vector<double> a)
    {
        two_body = std::move(a);
        return *this;
    }    

    std::vector<double> one_body;
    std::vector<double> two_body;
};

class Diagonalface
{
public:
    Diagonalface(std::size_t n) : backend(Diagonalface::create(n)), result() {}

    Diagonalface& configure(const Config& config)
    {
        backend->Configure(config.ham, config.up, config.down, config.steps);
        return *this;
    }
    Diagonalface& diagonalize(const Params& p)
    {
        result = backend->Diagonalize(p.one_body, p.two_body);
        return *this;
    }
    Diagonalface& print_stats()
    {
        backend->PrintStats();
        return *this;
    } 
    Diagonalface& get_result(double* out)
    {
        *out = result;
        return *this;
    }
private:
    static std::unique_ptr<IDiagonalass> create(std::size_t n) 
    { 
        return std::unique_ptr<IDiagonalass>(new Diagonalass(n)); 
    }
    std::unique_ptr<IDiagonalass> backend;
    double result;
};


class DiagonalizationProcedure
{
public:
    DiagonalizationProcedure(std::size_t n) 
        : backend(DiagonalizationProcedure::create(n))
        , params()
        , result() {}

    DiagonalizationProcedure& with(const Config& config)
    {
        backend->Configure(config.ham, config.up, config.down, config.steps);
        return *this;
    }
    DiagonalizationProcedure& with(const Params& p)
    {
        params = p;
        return *this;
    }
    DiagonalizationProcedure& run()
    {
        result = backend->Diagonalize(params.one_body, params.two_body);
        return *this;
    } 
    DiagonalizationProcedure& get_result(double* out)
    {
        *out = result;
        return *this;
    }
private:
    static std::unique_ptr<IDiagonalass> create(std::size_t n) 
    { 
        return std::unique_ptr<IDiagonalass>(new Diagonalass(n)); 
    }
    std::unique_ptr<IDiagonalass> backend;
    Params params;
    double result;
};

void use_case_no_1()
{
    std::vector<double> one_body = {1, 2, 3};
    std::vector<double> two_body = {1, 2, 3, 4};

    double result = 0;

    Diagonalface(2)
        .configure(Config().set_ham("hammer.dat")
                           .set_up(12)
                           .set_down(5)
                           .set_steps(10000))
        .diagonalize(Params().set_one_body(one_body)
                             .set_two_body(two_body))
        .get_result(&result)
        .print_stats();

    std::cout << "Result: " << result << '\n' << std::endl;
}

void use_case_no_2()
{
    std::vector<double> one_body = {1, 2, 3, 4, 5, 6, 7, 8};
    std::vector<double> two_body = {1, 2, 3, 4};

    double result = 0;

    Diagonalface(66)
        .configure(Config().set_down(666)
                           .set_steps(10000)
                           .set_up(555)
                           .set_ham("warhammer.dat"))
        .diagonalize(Params().set_two_body(two_body)
                             .set_one_body(one_body))
        .get_result(&result)
        .print_stats();

    std::cout << "Result: " << result << '\n' << std::endl;     
}

void use_case_no_3()
{
    using Diagonalization = Diagonalface;

    std::vector<double> one_body = {1, 1, 1, 1};
    std::vector<double> two_body = {1, 2};

    double result = 0;

    auto config = Config().set_down(1).set_up(2).set_steps(100).set_ham("heavy.data");
    auto params = Params().set_one_body(one_body).set_two_body(two_body);

    Diagonalization(1024).configure(config)
                         .diagonalize(params)
                         .get_result(&result);

    std::cout << "Result: " << result << '\n' << std::endl;     
}

void use_case_no_4()
{
    double result = 0;
     
    DiagonalizationProcedure(12)
        .with(Config())
        .with(Params())
        .run()
        .get_result(&result);

    std::cout << "Result: " << result << '\n' << std::endl;        
}

int main()
{
    use_case_no_1();
    use_case_no_2();
    use_case_no_3();
    use_case_no_4();
}